class User < ApplicationRecord
  enum role: [:admin, :guest, :owner]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :shop, foreign_key: :owner_id

  accepts_nested_attributes_for :shop
end
