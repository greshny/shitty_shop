class Shop < ApplicationRecord
  has_many :products, dependent: :destroy
  belongs_to :owner, foreign_key: :owner_id, class_name: 'User'

  validates_presence_of :name
end
