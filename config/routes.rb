Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }
  resources :shops
  resources :products
  root to: 'products#index'
end
