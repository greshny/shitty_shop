# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
User.create(email: 'root@example.com', password: '.test.', password_confirmation: '.test.')

10.times do
  Product.create(title: Faker::Hacker.adjective, description: Faker::Hacker.say_something_smart, photo: "http://fakeimg.pl/350x200/?text=#{Faker::Hacker.abbreviation}")
end
